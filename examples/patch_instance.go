package main

import "fmt"
import "net"
import "net/http"
import "reflect"

import "gitlab.com/ggpack/monkey"

func main() {
	var dialer *net.Dialer
	monkey.PatchInstanceMethod(reflect.TypeOf(dialer), "Dial", func(_ *net.Dialer, _, _ string) (net.Conn, error) {
		return nil, fmt.Errorf("no dialing allowed")
	})
	_, err := http.Get("http://google.com")
	fmt.Println(err)
}

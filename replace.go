package monkey

import "reflect"
import "syscall"
import "unsafe"

func rawMemoryAccess(ptr uintptr, length int) []byte {
	var ret []byte
	header := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
	header.Data = ptr
	header.Cap = length
	header.Len = length
	return ret
}

func pageStart(ptr uintptr) uintptr {
	return ptr & ^(uintptr(syscall.Getpagesize() - 1))
}

// from is a pointer to the actual function
// to is a pointer to a go funcvalue
func replaceFunction(from, to uintptr) (original []byte) {
	jumpData := jmpToFunctionValue(to)
	f := rawMemoryAccess(from, len(jumpData))
	original = make([]byte, len(f))
	copy(original, f)

	copyToLocation(from, jumpData)
	return
}

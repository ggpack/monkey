package main

import "fmt"
import "os"
import "gitlab.com/ggpack/monkey"

func printlnPatched(a ...interface{}) (n int, err error) {
	return fmt.Fprintln(os.Stdout, "hihi 🐵🐒 houhou 🐵!")
}

func main() {
	monkey.Patch(fmt.Println, printlnPatched)
	fmt.Println("What was that?  'go run -gcflags=-l patch.go'")
}

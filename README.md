# Go monkeypatching​ 🐒

Actual arbitrary monkeypatching for Go. Yes really.

Read this blogpost for an explanation on how it works: https://bou.ke/blog/monkey-patching-in-go/

## I thought that monkeypatching in Go is impossible?

It's not possible through regular language constructs, but we can always bend computers to our will! Monkey implements monkeypatching by rewriting the running executable at runtime and inserting a jump to the function you want called instead. **This is as unsafe as it sounds and I don't recommend anyone do it outside of a testing environment.**

Make sure you read the notes at the bottom of the README if you intend to use this library.

## Using monkey

Refer to [the example section](examples).

``` go
package main

import "fmt"
import "os"
import "strings"

import "gitlab.com/ggpack/monkey"
func printlnPatched(a ...interface{}) (n int, err error) {
	return fmt.Fprintln(os.Stdout, "hihi 🐵🐒 houhou 🐵!")
}

func main() {
	monkey.Patch(fmt.Println, printlnPatched)
	fmt.Println("What was that?  'go run -gcflags=-l patch.go'")
	// Confusing monkey noises
}
```


## Notes

1. Monkey sometimes fails to patch a local package function if inlining is enabled. Try running your tests with inlining disabled, for example: `go test -gcflags=-l`. The same command line argument can also be used for build.
2. Monkey won't work on some security-oriented operating system that don't allow memory pages to be both write and execute at the same time. With the current approach there's not really a reliable fix for this.
3. Monkey is not threadsafe. Or any kind of safe.
4. I've tested monkey on OSX 10.10.2 and Ubuntu 20.04. It should work on any unix-based x86 or x86-64 system.


# Thanks

[Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com) for the icon.

[© Bouke van der Bijl](bou.ke) for the initial sources.

## Original licence

Copyright Bouke van der Bijl

I do not give anyone permissions to use this tool for any purpose. Don't use it.

I’m not interested in changing this license. Please don’t ask.
